package com.example.knewit;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        
        Button button1 = (Button) findViewById(R.id.btnLogin);
	    
	    button1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
		      Intent i = new Intent(getApplicationContext(), ActivityTwo.class);
		      startActivity(i);
		     }	     
	     });
      
    }
    
}
